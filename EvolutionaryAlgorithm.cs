using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;


public class EvolutionaryAlgorithm : MonoBehaviour
{
    public GameObject player1Prefab;
    public GameObject player2Prefab;
    public GameObject resourcePrefab;

    public Terrain terrain;

    [Tooltip("The amount saved as the best from the simple micro + lambda")]
    public int micro;
    [Tooltip("The amount removed from the simple micro + lambda")]
    public int lamda;
    [Tooltip("The iterations of the algorithm")]
    public int n;
    [Tooltip("How many times the mutation shifts the cell")]
    public int mutateDistance = 1;


    public float playerDistanceWeight = 1f;
    public float playerResourceDistanceWeight = 1f;
    public float midpointDistanceWeight = 1f;

    int resources = 3;

    // Gene class to save generated grids
    private class Gene
    {
        public cell[,] grid;
        public float fitness;
        public float playerDistance;
        public float resourceDistance;
        public float midpointDistance;

        public Gene(cell[,] grid, float fitness)
        {
            this.grid = grid;
            this.fitness = fitness;
        }
    }

    // Types of valid cells 
    public enum cell
    {
        empty,
        player1,
        player2,
        resource
    }

    private cell[,] grid;
    private Gene[] population;

    int terrainWidth;
    int terrainLength;

    void Start()
    {
        // Start the simple micro + lambda array
        population = new Gene[micro+lamda];

        terrainWidth = (int)terrain.terrainData.size.x;
        terrainLength = (int)terrain.terrainData.size.z;

        // Create the grid with the size of the terrain
        grid = new cell[terrainWidth, terrainLength];


        // The starting type for the cell grids
        for (int i = 0; i < micro + lamda; i++)
        {
            for (int x = 0; x < terrainWidth; x++)
            {
                for (int y = 0; y < terrainLength; y++)
                {
                    grid[x, y] = cell.empty;

                    // Create and initialize the gene
                    population[i] = new Gene(new cell[terrainWidth, terrainLength], 0);
                }
            }
        }


        // Set the required cells 1 for each player and 3 resource points
        foreach (Gene gene in population)
        {
            RandomSetCell(gene, cell.player1);
            RandomSetCell(gene, cell.player2);

            for (int i = 0; i < resources; i++)
            {
                RandomSetCell(gene, cell.resource);
            }
        }

        Evolution(n, population);

        // Spawn into terrain based on the best gene of the population
        SpawnOnTerrain(population[0]);
    }

    
    void RandomSetCell(Gene gene, cell type)
    {
        // We randomly check for an empty space in the grid to asign the desired type
        while (true)
        {
            int x = UnityEngine.Random.Range(0, terrainWidth);
            int y = UnityEngine.Random.Range(0, terrainLength);

            if (gene.grid[x, y] == cell.empty)
            {
                gene.grid[x, y] = type;
                break;
            }
        }
    }

    void Evolution(int n, Gene[] population)
    {
        for (int generation = 0; generation < n; generation++)
        {
            // Calculate fitness for each gene
            foreach (Gene gene in population)
            {
                gene.fitness = Evaluate(gene);
            }

            // Sort the population by fitness
            Array.Sort(population, (a, b) => b.fitness.CompareTo(a.fitness));

            // Replace the worst lamda individuals with mutated clones of the best micro individuals
            for (int i = 0; i < lamda; i++)
            {
                int worstIndex = population.Length - i -1;
                int bestIndex = i % micro; // gets index valid in the micro amount
                population[worstIndex] = Clone(Mutate(population[bestIndex]));
            }
        }
    }

    // Mutate only one factor
    Gene Mutate(Gene gene)
    {
        bool cellFound = false;
        int newX = 0;
        int newY = 0;
        int x = 0;
        int y = 0;

        while (!cellFound)
        {
            x = UnityEngine.Random.Range(0, terrainWidth);
            y = UnityEngine.Random.Range(0, terrainLength);

            if (gene.grid[x, y] == cell.empty)
            {
                newX = x;
                newY = y;
                cellFound = true;
            }
        }


        // Randomly pick a shift direction
        int moveDirection = UnityEngine.Random.Range(0, 4);
        int distance = UnityEngine.Random.Range(1, mutateDistance);
        switch (moveDirection)
        {
            case 0: // Up
                newY = Mathf.Clamp(y - distance, 0, terrainLength-1);
                break;
            case 1: // Down
                newY = Mathf.Clamp(y + distance, 0, terrainLength-1);
                break;
            case 2: // Left
                newX = Mathf.Clamp(x - distance, 0, terrainWidth-1);
                break;
            case 3: // Right
                newX = Mathf.Clamp(x + distance, 0, terrainWidth-1);
                break;
            default:
                break;
        }

        // Cell swap
        cell temp = gene.grid[x, y];
        gene.grid[x, y] = gene.grid[newX, newY];
        gene.grid[newX, newY] = temp;

        x = newX; y = newY;

        return gene;
    }

    


    // Creates a clone creating a new gene using the values of another gene
    Gene Clone(Gene gene)
    {
        cell[,] cGrid = new cell[terrainWidth, terrainLength];

        for (int x = 0; x < terrainWidth; x++)
        {
            for (int y = 0; y < terrainLength; y++)
            {
                cGrid[x, y] = gene.grid[x, y];
            }
        }

        float Cfitness = gene.fitness;

        // Instantiate the clone gene
        return new Gene(cGrid, Cfitness);
    }

    float Evaluate(Gene gene)
    {
        // The fitness factors
        float playerDistanceFit = 0f;
        float resourceToPlayerDistanceFit = 0f;
        float midpointDistanceFit = 0f;

        #region 1- Get player distance Fit

        Vector2 player1 = Vector2.zero;
        Vector2 player2 = Vector2.zero;


        // Find the positions of player1 and player2
        for (int x = 0; x < terrainWidth; x++)
        {
            for (int y = 0; y < terrainLength; y++)
            {
                if (gene.grid[x, y] == cell.player1)
                {
                    player1.x = x;
                    player1.y = y;
                }
                else if (gene.grid[x, y] == cell.player2)
                {
                    player2.x = x;
                    player2.y = y;
                }
            }
        }

        // More distnace is better
        playerDistanceFit = EuclideanDistance(player1, player2)/(terrainLength*terrainWidth);
        #endregion

        #region 2- Get distance from player to resource

        // Use a list to stop repeating the cell resource being checked
        List<Vector2> resourcePositions = new List<Vector2>();

        for (int x = 0; x < terrainWidth; x++)
        {
            for (int y = 0; y < terrainLength; y++)
            {
                if (gene.grid[x, y] == cell.resource)
                {
                    resourcePositions.Add(new Vector2(x, y));
                }
            }
        }

        // Store the distances from resource to player
        Dictionary<Vector2, float> distancesToPlayer1 = new Dictionary<Vector2, float>();
        Dictionary<Vector2, float> distancesToPlayer2 = new Dictionary<Vector2, float>();

        // Player 1
        foreach (Vector2 resourcePosition in resourcePositions)
        {
            distancesToPlayer1[resourcePosition] = EuclideanDistance(resourcePosition, player1);
        }
        Vector2 player1Resource = distancesToPlayer1.OrderBy(kv => kv.Value).Last().Key;
        resourcePositions.Remove(player1Resource);

        // Player 2
        foreach (Vector2 resourcePosition in resourcePositions)
        {
            distancesToPlayer2[resourcePosition] = EuclideanDistance(resourcePosition, player2);
        }
        Vector2 player2Resource = distancesToPlayer2.OrderBy(kv => kv.Value).Last().Key;
        resourcePositions.Remove(player2Resource);

        // Closer is better
        resourceToPlayerDistanceFit = 1 / (distancesToPlayer2[player2Resource] + distancesToPlayer1[player1Resource]);

        #endregion

        #region 3- Get Midpoint distance 
        // How close to the mid point between the players is the midpoint resource
        Vector2 midpointResource = resourcePositions.First();

        // Players midpoint
        Vector2 midpointPlayers = new Vector2((player1.x + player2.x) / 2f, (player1.y + player2.y) / 2f);

        midpointDistanceFit = EuclideanDistance(midpointPlayers, midpointResource);

        // Closer distance is better
        midpointDistanceFit = 1 / midpointDistanceFit;
        #endregion

        //midpointDistanceFit = Mathf.Clamp(midpointDistanceFit, 0, 1);
        //resourceToPlayerDistanceFit = Mathf.Clamp(resourceToPlayerDistanceFit, 0, 1);
        playerDistanceFit = Mathf.Clamp(playerDistanceFit, 0, 1);

        gene.playerDistance = playerDistanceFit * playerDistanceWeight;
        gene.resourceDistance = resourceToPlayerDistanceFit * playerResourceDistanceWeight;
        gene.midpointDistance = midpointDistanceFit * midpointDistanceWeight;

        return midpointDistanceFit * midpointDistanceWeight +
               resourceToPlayerDistanceFit * playerResourceDistanceWeight +
               playerDistanceFit * playerDistanceWeight;

    }

    float EuclideanDistance(Vector2 point1, Vector2 point2)
    {
        return Mathf.Sqrt(Mathf.Pow(point2.x - point1.x, 2) + Mathf.Pow(point2.y - point1.y, 2)); ;
    }

    void SpawnOnTerrain(Gene gene)
    {
        Debug.Log("gene Fitness: " + gene.fitness + "\n PlayerDistance: " + gene.playerDistance +
                    "\n ResourceDistance: " + gene.resourceDistance +
                    "\n midpointDistance: " + +gene.midpointDistance);


        for (int x = 0; x < terrainWidth; x++)
        {
            for (int y = 0; y < terrainLength; y++)
            {
                Vector3 terrainPosition = terrain.transform.position;
                // Use y on Z position
                Vector3 position = terrainPosition + new Vector3(x, 0, y);

                if (gene.grid[x, y] == cell.player1)
                {
                    Instantiate(player1Prefab, position, Quaternion.identity);
                }
                else if (gene.grid[x, y] == cell.player2)
                {
                    Instantiate(player2Prefab, position, Quaternion.identity);
                }
                else if (gene.grid[x, y] == cell.resource)
                {
                    Instantiate(resourcePrefab, position, Quaternion.identity);
                }
            }
        }
    }

}
