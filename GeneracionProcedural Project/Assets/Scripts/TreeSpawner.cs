using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;

public class TreeSpawner : MonoBehaviour
{
    public GameObject treePrefab;
    public Terrain terrain;
    [Range(0.0f, 1.0f)]
    public float treeProbability = 0.5f;
    [Range(1, 10)]
    public int treeGenerations = 3;
    [Range(0.01f, 0.2f)]
    public float resolutionScale = 0.05f;
    public int neighbourhood = 5;
    public int neighbourhood_size = 1; // Moore

    private int[,] map;
    private int[,] mapAux;


    // Start is called before the first frame update
    void Start()
    {
        int width = (int)(terrain.terrainData.size.x * resolutionScale);
        int length = (int)(terrain.terrainData.size.z * resolutionScale);
        InitializeNeighbours(width, length);

        for (int i = 0; i < treeGenerations; i++) // Repite generaciones
        {
            GenerateCellularAutomataMap(width, length);
        }

        // Spawnear arboles
        SpawnTrees();
    }

    // Genera lista de posiciones 2D usando automatas celulares
    void GenerateCellularAutomataMap(int width, int length)
    {
        for (int i = 0; i < length; i++)
        {
            for (int j = 0; j < width; j++)
            {
                CheckNeighbours(i, j, width, length);
            }
        }

        // Modificar mapa original
        map = (int[,])mapAux.Clone();
    }

    // Revisa vecindario de Moore de un espacio en el arreglo y modifica mapAux acorde.
    void CheckNeighbours(int i, int j, int width, int length)
    {
        int counter = 0;
        for (int u = -neighbourhood_size; u <= neighbourhood_size; u++)
        {
            for (int v = -neighbourhood_size; v <= neighbourhood_size; v++)
            {
                if ((i + u >= 0 && i + u < length) && (j + v >= 0 && j + v < width)) // bordes
                {
                    if (map[i + u, j + v] == 1)
                    {
                        counter++;
                    }
                }
            }
        }
        if (counter >= neighbourhood) { mapAux[i, j] = 1; }
        else { mapAux[i, j] = 0; }
    }

    // Otorga valores al azar iniciales a map y mapAux
    void InitializeNeighbours(int width, int length)
    {
        map = new int[width, length];
        mapAux = new int[width, length];

        for (int i = 0; i < length; i++)
        {
            for (int j = 0; j < width; j++)
            {
                map[i, j] = (Random.Range(0f, 1f) < treeProbability) ? 1 : 0;
                mapAux[i, j] = 0;
            }
        }
    }

    // Instancia �rboles seg�n "map"
    // TODO: ARREGLAR PARA INTERSECTAR CON TERRAIN
    void SpawnTrees()
    {
        for (int i = 0; i < map.GetLength(0); i++)
        {
            for (int j = 0; j < map.GetLength(1); j++)
            {
                if (map[i, j] == 1)
                {
                    Vector3 spawnPosition = new Vector3(i * (1 / resolutionScale), 0, j * (1 / resolutionScale));
                    spawnPosition.y = terrain.SampleHeight(spawnPosition) - 0.05f;
                    Instantiate<GameObject>(treePrefab, spawnPosition, Quaternion.identity);

                }
            }
        }
    }


    // -DEBUG- GENERA ARREGLO DE ARBOLES DISTRIB. EN EL LARGO Y ANCHO DEL MAPA
    void SpawnTreeMatrixOverTerrain(int columns, int rows, float scale)
    {
        float width = terrain.terrainData.size.x;
        float length = terrain.terrainData.size.z;
        float norm_width = (columns != 1) ? ((width * scale) / (columns - 1)) : 0; // Se resta 1 ya que espacios_entre_arboles=arboles-1
        float norm_length = (rows != 1) ? ((length * scale) / (rows - 1)) : 0;

        for (int i = 0; i < columns; i++)
        {
            for (int j = 0; j < rows; j++)
            {
                Instantiate<GameObject>(treePrefab, new Vector3(i * norm_width, 0, j * norm_length), Quaternion.identity);
            }
        }
    }

    // -DEBUG- GENERA ARBOLES EN UN ARREGLO (separados por solo 1 world unit)
    void SpawnTreesFromOrigin(int columns, int rows)
    {
        for (int i = 0; i < columns; i++)
        {
            for (int j = 0; j < rows; j++)
            {
                Instantiate<GameObject>(treePrefab, new Vector3(i, 0, j), Quaternion.identity);
            }
        }
    }
}
