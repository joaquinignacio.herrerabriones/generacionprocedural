//using System;
using UnityEngine;

public class DiamondSquareTerrain : MonoBehaviour
{
    [Range(1, 9)]
    public int mapSizeExponent = 7;
    int mapSize = 129; // Map size (must be 2^n + 1)
    [Range(0.0f, 1.0f)]
    public float roughness = 1.0f; // Initial roughness
    [Range(0.0f, 1.0f)]
    public float roughnessDecrease = 0.5f;
    [Range(0.0f, 0.9f)]
    public float roughnessVariation = 0.2f;

    private float[,] originalHeights;
    private float[,] heightMap;
    //private Renderer terrainRenderer;
    Terrain terrain;

    void Awake()
    {
        if(roughness <= roughnessVariation)
        {
            Debug.LogWarning("roughnesVariation debe ser menor a roughness");
        }

        mapSize = (int)Mathf.Pow(2, mapSizeExponent) + 1;
        Debug.Log(mapSize);

        //terrainRenderer = GetComponent<Renderer>();
        terrain = GetComponent<Terrain>();

        originalHeights = new float[mapSize, mapSize];
        for(int i = 0; i < mapSize; i++)
        {
            for(int j = 0; j < mapSize; j++)
            {
                originalHeights[i, j] = 0;
            }
        }

        // Initialize the heightmap
        heightMap = new float[mapSize, mapSize];

        // Set initial heights at the corners
        heightMap[0, 0] = Random.value;
        heightMap[0, mapSize - 1] = Random.value;
        heightMap[mapSize - 1, 0] = Random.value;
        heightMap[mapSize - 1, mapSize - 1] = Random.value;

        // Generate the initial terrain
        GenerateTerrain();

        ApplyToTerrain();

        // Apply the heightmap to a texture and display the texture
        //ApplyToTexture();
    }

    void GenerateTerrain()
    {
        int stepSize = mapSize - 1;
        int count = 0;

        while (stepSize > 1)
        {
            int halfStep = stepSize / 2;

            // Diamond step
            for (int y = halfStep; y < mapSize; y += stepSize)
            {
                for (int x = halfStep; x < mapSize; x += stepSize)
                {
                    float percentage = (float)(x + y) / (float)(mapSize + mapSize); //El cast a float SI es necesario, de lo contrario da 0
                    float percentageSqr = percentage * percentage;
                    float modifiedRoughness = roughness - roughnessVariation + 2 * roughnessVariation * percentageSqr; //[r-rv, r+rv] 
                    if (count < 2) Debug.Log($"modifiedRoughness: {modifiedRoughness} | x: {x} | y: {y} | ({percentageSqr}%)");
                    DiamondStep(x, y, halfStep, Random.Range(-modifiedRoughness, modifiedRoughness));
                }
            }

            // Square step
            for (int y = 0; y < mapSize; y += halfStep)
            {
                for (int x = (y + halfStep) % stepSize; x < mapSize; x += stepSize)
                {
                    float percentage = (float)(x + y) / (float)(mapSize + mapSize); //El cast a float SI es necesario, de lo contrario da 0
                    float percentageSqr = percentage * percentage;
                    float modifiedRoughness = roughness - roughnessVariation + 2 * roughnessVariation * percentageSqr; //[r-rv, r+rv]
                    if (count < 2) Debug.Log($"modifiedRoughness: {modifiedRoughness} | x: {x} | y: {y} | ({percentageSqr}%)");
                    SquareStep(x, y, halfStep, Random.Range(-modifiedRoughness, modifiedRoughness));
                }
            }

            stepSize /= 2;
            roughness *= roughnessDecrease;
            roughnessVariation *= roughnessDecrease;

            count++;
        }
    }

    void DiamondStep(int x, int y, int stepSize, float offset)
    {
        float average = (heightMap[x - stepSize, y - stepSize] +
                         heightMap[x + stepSize, y - stepSize] +
                         heightMap[x - stepSize, y + stepSize] +
                         heightMap[x + stepSize, y + stepSize]) * 0.25f;

        heightMap[x, y] = Mathf.Clamp01(average + offset);
    }

    void SquareStep(int x, int y, int stepSize, float offset)
    {
        float sum = 0.0f;
        int count = 0;

        if (x - stepSize >= 0)
        {
            sum += heightMap[x - stepSize, y];
            count++;
        }
        if (x + stepSize < mapSize)
        {
            sum += heightMap[x + stepSize, y];
            count++;
        }
        if (y - stepSize >= 0)
        {
            sum += heightMap[x, y - stepSize];
            count++;
        }
        if (y + stepSize < mapSize)
        {
            sum += heightMap[x, y + stepSize];
            count++;
        }

        heightMap[x, y] = Mathf.Clamp01(sum / count + offset);
    }

    void ApplyToTerrain()
    {
        terrain.terrainData.SetHeights(0, 0, heightMap);
    }

    private void OnDisable()
    {
        terrain.terrainData.SetHeights(0, 0, originalHeights);
    }

    void ApplyToTexture()
    {
        Texture2D texture = new Texture2D(mapSize, mapSize);

        for (int y = 0; y < mapSize; y++)
        {
            for (int x = 0; x < mapSize; x++)
            {
                float heightValue = heightMap[x, y];
                heightValue = Mathf.Clamp01(heightValue);
                Color pixelColor = new Color(heightValue, heightValue, heightValue);
                texture.SetPixel(x, y, pixelColor);
            }
        }

        texture.Apply();
        //terrainRenderer.material.mainTexture = texture;
    }
}
