using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using UnityEngine;

public class BracketedL_System : MonoBehaviour
{
    [Tooltip("The number of iterations")]
    public int n = 1;
    [Tooltip("The rotation angle in degrees")]
    public float angle = 25f;
    [Tooltip("Length of each line segment")]
    public float scale = 0.25f;
    [Tooltip("The starting axiom")]
    public string axiom = "F";
    [Tooltip("The should it randomized factors of the tree")]
    public bool randomize = true;

    float growMultiplier = 1.2f;

    public GameObject linePrefab;
    public GameObject branchPrefab;
    public GameObject leavesPrefab;

    char[] validAxioms; // to draw straight when getting an axiom

    public Rules[] rules; // Array of axiom and rule pairs
    [System.Serializable]
    public struct Rules
    {
        public char variable;
        public string rule;

        public Rules(char var, string r)
        {
            variable = var;
            rule = r;
        }
    }

    private Stack<TransformState> transformStack; // Stack to save the transform of the line
    private struct TransformState
    {
        public Vector3 position;
        public Quaternion rotation;

        public TransformState(Vector3 pos, Quaternion rot)
        {
            position = pos;
            rotation = rot;
        }
    }

    private void Start()
    {
        if(randomize) RandomizeTree();

        // Initialize the stack for transform states
        transformStack = new Stack<TransformState>();

        // We set the validAxioms
        validAxioms = rules.Select(ar => ar.variable).ToArray();

        // The final string to be drawn starts with the first axiom
        string finalString = axiom;

        // Apply the rules for N iterations
        for (int i = 0; i < n; i++)
        {
            finalString = ApplyRules(finalString);
        }

        // The final string is generated
        Write(finalString);
        Generate(finalString);
    }

    void RandomizeTree()
    {
        // Change the iterations
        n = Random.Range(1, 4);

        // Change characters in the rule
        for (int i = 0; i < rules.Length; i++)
        {
            int index = Random.Range(1, rules[i].rule.Length); // Random index in the rule.rule string
            switch (Random.Range(0, 4))
            {
                case 0:
                    rules[i].rule = ReplaceCharAtIndex(rules[i].rule, index, ';');
                    break;
                case 1:
                    rules[i].rule = ReplaceCharAtIndex(rules[i].rule, index, ':');
                    break;
                case 2:
                    rules[i].rule = ReplaceCharAtIndex(rules[i].rule, index, '+');
                    break;
                case 3:
                    rules[i].rule = ReplaceCharAtIndex(rules[i].rule, index, '-');
                    break;
                default:
                    rules[i].rule += ReplaceCharAtIndex(rules[i].rule, index, rules[i].variable);
                    break;
            }
        }
    }

    // Function to replace a character at index
    string ReplaceCharAtIndex(string oldString, int index, char newChar)
    {
        if (index < 0 || index >= oldString.Length) return oldString;

        char[] charArray = oldString.ToCharArray();
        if(charArray[index] != '[' & charArray[index] != ']') charArray[index] = newChar;
        return new string(charArray);
    }


    string ApplyRules(string input)
    {
        string result = "";
        foreach (char c in input)
        {
            // Find the corresponding rule for the axiom
            Rules rule = rules.FirstOrDefault(ar => ar.variable == c);

            if (rule.variable == c)
            {
                result += rule.rule;
            }
            else
            {
                result += c;
            }
        }

        return result.ToString();
    }

    private void Generate(string currentString)
    {
        int branchCount = 0;
        float leafScale = 1f;

        // Parent of tree
        GameObject parentObject = new GameObject("Tree");
        // Set an initial position at the base of the tree
        Vector3 currentPosition = transform.position;
        // Starting Rotation
        transform.rotation = Quaternion.identity;
        // LastCreatedLeaf
        GameObject recentLeaf = new GameObject("Leaf");
        // List to keep track of leaf prefabs that will be destroyed (not end branches)
        List<GameObject> nonBranchLeaves = new List<GameObject>();

        for (int i = 0; i < currentString.Length; i++)
        {

            char c = currentString[i];

            // Draws a straight line
            if (validAxioms.Contains(c))
            {
                // Instantiate a branch model
                GameObject newBranch = Instantiate(branchPrefab, currentPosition, transform.rotation);
                newBranch.transform.parent = parentObject.transform;

                // Adjust the position upwards (along the Y-axis)
                currentPosition += newBranch.transform.up * newBranch.GetComponentInChildren<Collider>().bounds.size.y * scale;

                // Scale the branch
                newBranch.transform.localScale *= scale;

                // Instantiate a leaf at the new current position
                GameObject newLeaf = Instantiate(leavesPrefab, currentPosition, transform.rotation);
                newLeaf.transform.parent = parentObject.transform;
                newLeaf.transform.localScale *= (scale * leafScale);
                recentLeaf = newLeaf;

                nonBranchLeaves.Add(recentLeaf);

                currentPosition = newLeaf.transform.position;

                // if stack is empty, the leaves grow
                if (transformStack.Count == 0)
                {
                    leafScale *= growMultiplier;
                }
            }

            // Rotates around the Y-axis to the right
            else if (c == '-')
            {
                transform.Rotate(0, angle, 0, Space.Self);
            }

            // Rotates around the Y-axis to the left
            else if (c == '+')
            {
                transform.Rotate(0, -angle, 0, Space.Self);
            }

            // Rotates around the Z-axis to the right
            else if (c == ':')
            {
                transform.Rotate(0, 0, angle, Space.Self);
            }

            // Rotates around the Z-axis to the left
            else if (c == ';')
            {
                transform.Rotate(0, 0, -angle, Space.Self);
            }

            // Saves transform state
            else if (c == '[')
            {
                branchCount++;
                transformStack.Push(new TransformState(currentPosition, transform.rotation));
            }

            // Returns to saved transform state
            else if (c == ']')
            {
                if (transformStack.Count > 0)
                {
                    // We are at the end of a branch so we remove the GameObject 
                    nonBranchLeaves.Remove(recentLeaf);

                    // Go back to a previous Transform
                    TransformState recall = transformStack.Pop();
                    currentPosition = recall.position;
                    transform.rotation = recall.rotation;
                }
            }
        }

        // Is at the end of the tree
        GameObject lastLeaf = Instantiate(leavesPrefab, currentPosition, transform.rotation);
        lastLeaf.transform.parent = parentObject.transform;
        lastLeaf.transform.localScale *= (scale * leafScale);

        // Destroy all leaf prefabs that are not at the end of branches
        foreach (GameObject leaf in nonBranchLeaves)
        {
            Destroy(leaf);
        }
    }

    void Write(string stringArray)
    {
        Debug.Log("Final String: " + stringArray);
    }
}
