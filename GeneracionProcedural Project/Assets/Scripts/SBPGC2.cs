using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;

public class SBPGC2 : MonoBehaviour
{
    public GameObject player1Prefab;
    public GameObject player2Prefab;
    public GameObject resourcePrefab;
    public Terrain terrain;

    [Tooltip("The size of the population that will evolve")]
    public int populationSize;
    [Tooltip("The amount of resources to be spawned")]
    public int resources = 3;
    [Tooltip("Amount of generations that will evovle")]
    public int generations = 10;
    [Tooltip("Change in percent for a gene to mutate")]
    public float mutationChance = 10f;
    [Tooltip("The amount of units the mutation moves")]
    public float mutationMove = 5f;
    [Tooltip("Weight for evaluation function")]
    public float playerResourceDistanceWeight = 1f;
    [Tooltip("Weight for evaluation function")]
    public float resourceDistanceWeight = 1f;
    [Tooltip("Weight for evaluation function")]
    public float playerDistanceCornerWeight = 1f;


    int terrainWidth;
    int terrainLength;

    public enum positionType
    {
        player1,
        player2,
        resource
    }

    public class PositionedObject
    {
        public Vector3 position;
        public positionType type;

        public PositionedObject(Vector3 position, positionType type)
        {
            this.position = position;
            this.type = type;
        }
    }

    List<List<PositionedObject>> population;

    void Start()
    {
        // Obtain dimensions to Clamp and avoid going out of terrain
        terrainWidth = (int)terrain.terrainData.size.x;
        terrainLength = (int)terrain.terrainData.size.z;

        // Starting population
        population = InitializePopulation();

        // There is a single population that is mutated and fitted
        for (int gen = 0; gen < generations; gen++)
        {
            population = Evolution(population);
        }

        // Spawn the one with best fitness
        SpawnOnTerrain(GetBestLayout(population));
    }

    List<List<PositionedObject>> InitializePopulation()
    {
        var initialPopulation = new List<List<PositionedObject>>();

        for (int i = 0; i < populationSize; i++)
        {
            // Create layout with required positionedObjects
            List<PositionedObject> layout = new List<PositionedObject>();

            for (int j = 0; j < 2 + resources; j++)
            {
                Vector3 randomPosition = GetRandomPositionInTerrain();
                if (j == 0) layout.Add(new PositionedObject(randomPosition, positionType.player1));
                if (j == 1) layout.Add(new PositionedObject(randomPosition, positionType.player2));
                if (j > 1)layout.Add(new PositionedObject(randomPosition, positionType.resource));
            }

            // Add new layout to population
            initialPopulation.Add(layout);
        }

        return initialPopulation;
    }

    List<List<PositionedObject>> Evolution(List<List<PositionedObject>> currentPopulation)
    {
        var newPopulation = new List<List<PositionedObject>>();

        foreach (var layout in currentPopulation)
        {
            float mutatedFitness = float.MinValue;
            float currentFitness = float.MinValue;

            var originalLayout = layout;
            currentFitness = Evaluation(layout);
            
            // Chance to mutate
            if (UnityEngine.Random.value < mutationChance/100f)
            {
                Mutate(layout);
                mutatedFitness = Evaluation(layout);
            }

            //Depending on fitness add the mutated or original version
            if (mutatedFitness > currentFitness) newPopulation.Add(layout);
            else newPopulation.Add(originalLayout);

        }

        return newPopulation;
    }

    void Mutate(List<PositionedObject> layout)
    {
        int randomIndex = UnityEngine.Random.Range(0, layout.Count);
        layout[randomIndex].position = MovePositionInTerrain(layout[randomIndex].position);
    }

    Vector3 MovePositionInTerrain(Vector3 position)
    {
        // Move randomly in the terrain bounds
        float randomX = UnityEngine.Random.Range(-mutationMove, mutationMove);
        float newX = Mathf.Clamp(position.x + randomX, 0, terrainWidth);

        float randomY = UnityEngine.Random.Range(-mutationMove, mutationMove);
        float newZ = Mathf.Clamp(position.x + randomX, 0, terrainLength);

        return new Vector3(newX, 0f, newZ);

    }

    Vector3 GetRandomPositionInTerrain()
    {
        // Gets random position within terrain bounds
        float randomX = UnityEngine.Random.Range(0f, terrainWidth);
        float randomZ = UnityEngine.Random.Range(0f, terrainLength);

        Vector3 randomPosition = new Vector3(randomX, 0f, randomZ);

        return randomPosition;
    }

    float Evaluation(List<PositionedObject> layout)
    {
        float resourceDistanceFit = 0f;
        float playerResourceDistanceFit = 0f;
        float playerDistanceToCorner = 0f;

        List<PositionedObject> players = new List<PositionedObject>();
        List<PositionedObject> resources = new List<PositionedObject>();

        // Find Evaluation Positions
        foreach (var positionedObject in layout)
        {
            if (positionedObject.type == positionType.player1 || positionedObject.type == positionType.player2)
            {
                players.Add(positionedObject);
            }
            else if (positionedObject.type == positionType.resource)
            {
                resources.Add(positionedObject);
            }
        }

        // Get distance from resources. The bigger the better
        for (int i = 0; i < resources.Count; i++)
        {
            for (int j = i + 1; j < resources.Count; j++)
            {
                resourceDistanceFit += Vector3.Distance(resources[i].position, resources[j].position);
            }
        }

        // Find the distance from a players to each closest resource
        foreach (var player in players)
        {
            float closestDistance = float.MaxValue;

            foreach (var resource in resources)
            {
                float distanceFromResourceToPlayer = Vector3.Distance(player.position, resource.position);
                if (distanceFromResourceToPlayer < closestDistance)
                {
                    closestDistance = distanceFromResourceToPlayer;
                }
            }

            playerResourceDistanceFit += closestDistance;
        }
        playerResourceDistanceFit *= 0.1f; // The samller the better

        // Find distance from player to corners
        foreach (var player in players)
        {
            float distanceToTopLeftCorner = Vector3.Distance(player.position, new Vector3(0, 0, 0));
            float distanceToTopRightCorner = Vector3.Distance(player.position, new Vector3(terrainWidth, 0, 0));
            float distanceToBottomLeftCorner = Vector3.Distance(player.position, new Vector3(0, 0, terrainLength));
            float distanceToBottomRightCorner = Vector3.Distance(player.position, new Vector3(terrainWidth, 0, terrainLength));

            // Normalize distances
            distanceToTopLeftCorner /= terrainWidth + terrainLength;
            distanceToTopRightCorner /= terrainWidth + terrainLength;
            distanceToBottomLeftCorner /= terrainWidth + terrainLength;
            distanceToBottomRightCorner /= terrainWidth + terrainLength;

            playerDistanceToCorner += distanceToTopLeftCorner + distanceToTopRightCorner + distanceToBottomLeftCorner + distanceToBottomRightCorner;
        }
        playerDistanceToCorner *= 0.1f; // The smaller the better

        // Normalize distances
        playerResourceDistanceFit /= terrainWidth + terrainLength;
        resourceDistanceFit /= terrainWidth + terrainLength;

        float fitness = resourceDistanceFit * resourceDistanceWeight +
                        playerResourceDistanceFit * playerResourceDistanceWeight + 
                        playerDistanceToCorner * playerDistanceCornerWeight;

        return fitness;
    }

    List<PositionedObject> GetBestLayout(List<List<PositionedObject>> layoutPopulation)
    {
        List<PositionedObject> bestLayout = null;
        float bestFitness = float.MinValue;

        foreach (var layout in layoutPopulation)
        {
            float fitness = Evaluation(layout);

            if (fitness > bestFitness)
            {
                bestFitness = fitness;
                bestLayout = layout;
            }
        }

        return bestLayout;
    }

    void SpawnOnTerrain(List<PositionedObject> layout)
    {
        foreach (var positionedObject in layout)
        {
            Vector3 position = positionedObject.position;
            positionType type = positionedObject.type;

            if (type == positionType.player1)
            {
                Instantiate(player1Prefab, position, Quaternion.identity);
            }
            else if (type == positionType.player2)
            {
                Instantiate(player2Prefab, position, Quaternion.identity);
            }
            else if (type == positionType.resource)
            {
                Instantiate(resourcePrefab, position, Quaternion.identity);
            }
        }
    }
}




