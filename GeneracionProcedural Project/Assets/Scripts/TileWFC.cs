using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileWFC : MonoBehaviour
{
    public int ID = -1;

    public List<GameObject> UP;
    public List<GameObject> DOWN;
    public List<GameObject> RIGHT;
    public List<GameObject> LEFT;

    // Use to access the valid connection paths of a tile
    public List<Direction> availablePaths = new List<Direction>();

    public enum Direction
    {
        Up,
        Down,
        Left,
        Right
    }
    
    // whether the tile allows to exit the map
    public bool Exit = false;
    public bool Required = false;

    public bool hasCar = false;

    public bool IsStreet => availablePaths.Count > 0;

    private void Start()
    {
        if(ID == -1) { Debug.LogError("Falta asignar ID a prefab de tile"); }
    }
}
