using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;

public class WFC : MonoBehaviour
{

    public GameObject[] tiles;

    public static GameObject[] staticTiles;

    public static GameObject GetTileByID(int id)
    {
        //int b = 0;
        /*
        foreach(GameObject tile in staticTiles)
        {
            Debug.Log($"busqueda {b}");
            b++;
            if (tile.ID == id)
            {
                return tile;
            }
        }
        */
        if (id < 0 || id >= staticTiles.Length) return null;
        return staticTiles[id];
    }

    // For Grid size
    public int rows;
    public int columns;

    public int maxRetries = 10;

    // Grid with collapse data
    public Grid grid;

    // list with states
    List<GridCell[,]> gridStateHistory = new List<GridCell[,]>();
    // set to keep track of visited states
    private HashSet<int> visitedStates = new HashSet<int>();
    // state index
    int gridStateIndex = 0;

    Vector2Int invalidPosition = new Vector2Int(-1, -1);

    public class GridCell
    {
        // List of possible tiles for this cell
        // Entropy.Count = Entropy
        public List<GameObject> Entropy;

        public GameObject Tile;
        public int tileID;

        public bool isStart = false;
        public bool isGoal = false;
        public bool hasCar = false;

        public int x;
        public int y;

        public List<TileWFC.Direction> directions = new List<TileWFC.Direction>();

        [HideInInspector]
        public bool explorado = false;

        public bool isStreet = false;

        public GridCell()
        {
            Entropy = new List<GameObject>();
        }

        public GridCell(GridCell toClone)
        {
            if (toClone.Tile is null) { Debug.LogError("TILE NULO"); }
            Tile = GetTileByID(toClone.tileID);
            tileID = toClone.tileID;
            isStart = toClone.isStart;
            isGoal = toClone.isGoal;
            hasCar = toClone.hasCar;
            x = toClone.x;
            y = toClone.y;

            directions = new List<TileWFC.Direction>(toClone.directions);

            isStreet = toClone.isStreet;
        }

        public override bool Equals(object obj)
        {
            if(obj is null) return false;
            GridCell other = obj as GridCell;
            if(other is null) return false;
            return x == other.x && y == other.y;
        }
    }

    public class Grid : IEquatable<object>, IComparable<object>
    {
        static int nextValidID = 1;
        public int id = 0;

        public int Rows, Columns;
        public GridCell[,] Cells;

        public float ESFitness;

        public Grid(int rows, int columns)
        {
            Rows = rows; Columns = columns;
            Cells = new GridCell[rows, columns];

            for (int row = 0; row < rows; row++)
            {
                for (int col = 0; col < columns; col++)
                {
                    Cells[row, col] = new GridCell();
                }
            }
        }

        public Grid(Grid toClone)
        {
            id = nextValidID;
            nextValidID++;

            Rows = toClone.Rows;
            Columns = toClone.Columns;
            Cells = new GridCell[Rows, Columns];
            for (int row = 0; row < Rows; row++)
            {
                for (int col = 0; col < Columns; col++)
                {
                    Cells[row, col] = new GridCell(toClone.Cells[row, col]);
                }
            }
        }

        // (GABO) Single move (or "mutation")
        public void MoveOnceRandomly()
        {
            // Total Street tiles?
            int totalStreetTiles = 0;
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    if (Cells[i, j].isStreet)
                    {
                        totalStreetTiles++;
                    }
                }
            }
            // Check each empty (NoCar/NonStart/NonGoal) street tile till valid move is made
            int streetsToIgnore = Random.Range(0, totalStreetTiles) - 1;
            int streetsChecked = 0;

            while (true)
            {
                // Check for error (no valid moves?)
                if (streetsChecked >= totalStreetTiles) {
                    Debug.LogError("No valid moves found!");
                    Debug.Break();
                    throw new Exception();
                }

                // Choose one empty street
                int streetCounter = streetsToIgnore;
                GridCell currCell = new GridCell();
                for (int i = 0; i < Rows; i++)
                {
                    bool streetChosen = false;
                    for (int j = 0; j < Columns; j++)
                    {
                        if (Cells[i, j].isStreet &&
                            !Cells[i, j].hasCar &&
                            !Cells[i, j].isStart &&
                            !Cells[i, j].isGoal &&
                            streetCounter > 0)
                        {
                            streetCounter--;
                            continue;
                        }
                        else if (Cells[i, j].isStreet &&
                            !Cells[i, j].hasCar &&
                            !Cells[i, j].isStart &&
                            !Cells[i, j].isGoal &&
                            streetCounter == 0)
                        {
                            currCell = Cells[i, j];
                            streetChosen = true;
                            break;
                        }
                    }
                    if (streetChosen) break;
                }

                // Try pulling one of the sides
                int sideCounter = 0;
                int side = Random.Range(1, 5);
                List<TileWFC.Direction> currPaths = currCell.directions;
                while (sideCounter < 4)
                {
                    // UPPER SIDE
                    if (side == 1 && currCell.y > 0 && currPaths.Contains(TileWFC.Direction.Up))
                    {
                        GridCell upperNeighbour = Cells[currCell.y - 1, currCell.x];
                        if (upperNeighbour.hasCar)
                        {
                            upperNeighbour.hasCar = false;
                            currCell.hasCar = true;
                            return;
                        }
                    }
                    // LOWER SIDE
                    else if (side == 2 && currCell.y < Rows - 1 && currPaths.Contains(TileWFC.Direction.Down))
                    {
                        GridCell lowerNeighbour = Cells[currCell.y + 1, currCell.x];
                        if (lowerNeighbour.hasCar)
                        {
                            lowerNeighbour.hasCar = false;
                            currCell.hasCar = true;
                            return;
                        }
                    }
                    // LEFT SIDE
                    else if (side == 3 && currCell.x > 0 && currPaths.Contains(TileWFC.Direction.Left))
                    {
                        GridCell leftNeighbour = Cells[currCell.y, currCell.x - 1];
                        if (leftNeighbour.hasCar)
                        {
                            leftNeighbour.hasCar = false;
                            currCell.hasCar = true;
                            return;
                        }
                    }
                    // RIGHT SIDE
                    else if (side == 4 && currCell.x < Columns - 1 && currPaths.Contains(TileWFC.Direction.Right))
                    {
                        GridCell rightNeighbour = Cells[currCell.y, currCell.x + 1];
                        if (rightNeighbour.hasCar)
                        {
                            rightNeighbour.hasCar = false;
                            currCell.hasCar = true;
                            return;
                        }
                    }
                    // Check next side. Wrap around side==4 back to 1 (if any sides are pending)
                    side = (side < 4) ? side + 1 : 1;
                    sideCounter++;
                }
                // Check next street. Wrap streetsToIgnore around
                streetsToIgnore = (streetsToIgnore == totalStreetTiles) ? 0 : streetsToIgnore + 1;
                streetsChecked++;
            }
        }

        // Get ortogonal positions
        public Vector2Int GetUpPosition(Vector2Int currentPosition)
        {
            int upRow = currentPosition.x - 1;
            return (upRow >= 0) ? new Vector2Int(upRow, currentPosition.y) : currentPosition;
        }

        public Vector2Int GetDownPosition(Vector2Int currentPosition)
        {
            int downRow = currentPosition.x + 1;
            return (downRow < Rows) ? new Vector2Int(downRow, currentPosition.y) : currentPosition;
        }

        public Vector2Int GetLeftPosition(Vector2Int currentPosition)
        {
            int leftColumn = currentPosition.y - 1;
            return (leftColumn >= 0) ? new Vector2Int(currentPosition.x, leftColumn) : currentPosition;
        }

        public Vector2Int GetRightPosition(Vector2Int currentPosition)
        {
            int rightColumn = currentPosition.y + 1;
            return (rightColumn < Columns) ? new Vector2Int(currentPosition.x, rightColumn) : currentPosition;
        }

        public override bool Equals(object obj)
        {
            if (obj is null || !(obj is Grid)) return false;
            return id.Equals((obj as Grid).id);
        }

        public int CompareTo(object obj)
        {
            return ESFitness.CompareTo((obj as Grid).ESFitness);
        }

        public override string ToString()
        {
            string val = "";
            for (int y = 0; y < Rows; y++)
            {
                for (int x = 0; x < Columns; x++)
                {
                    string nodo = "-";
                    if (Cells[y, x].isStart)
                    {
                        nodo = "I";
                    }
                    else if (Cells[y, x].isGoal)
                    {
                        nodo = "F";
                    }
                    else if (Cells[y, x].isStreet&& Cells[y,x].hasCar)
                    {
                        nodo = "C";
                    }
                    else if (Cells[y, x].isStreet)
                    {
                        nodo = " ";
                    }
                    else
                    {
                        nodo = "X";
                    }

                    val += nodo;
                }
                val += "\n";
            }
            return val;
        }
    }


    public void Start()
    {
        staticTiles = new GameObject[tiles.Length];
        for (int i = 0; i < tiles.Length; i++)
        {
            staticTiles[i] = tiles[i];
        }

        grid = new Grid(rows, columns);

        GenerateWFC();
    }

    public void GenerateWFC()
    {
        // Set up the grid initially
        SetUpGrid();
        while (maxRetries > 0)
        {
            bool success = WFCGeneration();
            if (success)
            {
                // if succeessful spawn
                SpawnGridCells(grid);
                // Set valid exits 
                SetExits();
                FindObjectOfType<ES_Cars>().StartES(this);
                return;
            }

            // REset the grid and try again from Zero
            gridStateHistory.Clear();
            SetUpGrid();

            maxRetries--;
        }

        // If fails to generate content
        UnityEngine.Debug.LogError("WFC generation failed after maximum retries.");
    }

    // Add current grid state into the StateHistory
    private void SaveGridState()
    {
        GridCell[,] currentState = new GridCell[grid.Rows, grid.Columns];
        for (int x = 0; x < grid.Rows; x++)
        {
            for (int y = 0; y < grid.Columns; y++)
            {
                currentState[x, y] = new GridCell();
                currentState[x, y].Tile = grid.Cells[x, y].Tile;
                currentState[x, y].Entropy = new List<GameObject>(grid.Cells[x, y].Entropy);
            }
        }
        gridStateHistory.Add(currentState);
        // Update the index var
        gridStateIndex = gridStateHistory.Count - 1;
    }

    // Sets the grid back to a previous state contained in the StateHistory
    private void Backtrack()
    {
        if (gridStateIndex > 0)
        {
            gridStateIndex--;
            grid.Cells = gridStateHistory[gridStateIndex];
            UnityEngine.Debug.Log($"Backtracking to index: {gridStateIndex}");

            // Add the current state to a set of visited states
            visitedStates.Add(gridStateIndex);

            // Check if you've visited this state before
            if (visitedStates.Contains(gridStateIndex))
            {
                UnityEngine.Debug.Log("Detected a loop in state history, stopping backtracking.");
                // You can add more logic here, like breaking out of the loop or taking a different action.
            }
        }
    }


    public bool WFCGeneration()
    {
        int maxIterations = 100000;

        #region BuildPropagator
        // Set the first cell at random
        int x = UnityEngine.Random.Range(0, rows);
        int y = UnityEngine.Random.Range(0, columns);
        Vector2Int startPos = new Vector2Int(x, y);

        SetTile(startPos);
        #endregion


        while (maxIterations > 0)
        {
            #region Observe
            // if all sets have entropy 0
            if (AllTilesSet()) return true;

            Vector2Int cellPos = GetMinEntropyCell();
            #endregion

            #region Propagation
            // CellPos is invalid, so backtrack.
            if (cellPos == invalidPosition) Backtrack();

            // Cell has entropy greater than 0.
            else if (grid.Cells[cellPos.x, cellPos.y].Entropy.Count > 0)
            {  
                SetTile(cellPos);
                SaveGridState();
            }

            // When minimal Entropy is 0 without tile being set
            else Backtrack();

            #endregion

            maxIterations--;
        }

        return false;
    }


    // Sets tile at a position, entropy to 0 and Updates Neighbors
    private void SetTile(Vector2Int cellPos)
    {
        grid.Cells[cellPos.x, cellPos.y].Tile = grid.Cells[cellPos.x, cellPos.y].Entropy[UnityEngine.Random.Range(0, grid.Cells[cellPos.x, cellPos.y].Entropy.Count)];
        TileWFC selectedTile = grid.Cells[cellPos.x, cellPos.y].Tile.GetComponent<TileWFC>();
        grid.Cells[cellPos.x, cellPos.y].Entropy.Clear();

        UpdateNeighborEntropy(cellPos);
    }

    // Sets max entropy to all cells in grid and clears Tile
    public void SetUpGrid()
    {
        foreach (GridCell cell in grid.Cells)
        {
            cell.Entropy.Clear();
            foreach (GameObject tile in tiles)
            {
                cell.Entropy.Add(tile);
                cell.Tile = null;
            }
        }

        // We save the starting state
        SaveGridState();
    }

    // true if all tiles are set, therefore entropy 0 on all cells & required tiles exist in grid
    public bool AllTilesSet()
    {
        foreach (GridCell cell in grid.Cells)
        {
            if (cell.Tile == null) return false; // Therefore its entropy is 0

        }


        List<GameObject> requiredList = new List<GameObject>();
        foreach (GameObject requiredTile in tiles)
        {
            if (requiredTile.GetComponent<TileWFC>().Required) requiredList.Add(requiredTile);
        }

        List<GameObject> existingTiles = new List<GameObject>();
        foreach (GridCell cell in grid.Cells)
        {
            existingTiles.Add(cell.Tile);
        }

        foreach (GameObject required in requiredList)
        {
            if (!existingTiles.Contains(required))
            {
                return false; // A required tile is missing the generation is useless.
            }
        }

        return true;
    }

    // returns position of minimal entropy
    public Vector2Int GetMinEntropyCell()
    {
        Vector2Int minEntropyCellPosition = invalidPosition;

        // Initialize a list to keep track of cells with minimum entropy and empty tile
        List<Vector2Int> cellsPosWithMinEntropy = new List<Vector2Int>();

        // min entropy starts with the max amount of tiles
        int minEntropy = tiles.Length;

        for (int x = 0; x < grid.Rows; x++)
        {
            for (int y = 0; y < grid.Columns; y++)
            {
                if (grid.Cells[x, y].Tile == null)
                {

                    int entropy = grid.Cells[x, y].Entropy.Count;

                    // Check if the tile is null
                    if (entropy < minEntropy)
                    {
                        minEntropy = entropy;
                        cellsPosWithMinEntropy.Clear();
                        cellsPosWithMinEntropy.Add(new Vector2Int(x, y));
                    }
                    else if (entropy == minEntropy)
                    {
                        cellsPosWithMinEntropy.Add(new Vector2Int(x, y));
                    }
                }
            }
        }

        // If there are multiple cells with minimum entropy, choose one randomly
        if (cellsPosWithMinEntropy.Count > 0)
        {
            minEntropyCellPosition = cellsPosWithMinEntropy[UnityEngine.Random.Range(0, cellsPosWithMinEntropy.Count)];
        }

        return minEntropyCellPosition;
    }

    // Update the entropy of neighbor of a cell position
    public void UpdateNeighborEntropy(Vector2Int cellPos)
    {
        if (grid.Cells[cellPos.x, cellPos.y].Tile == null) return;

        // Access the component that contains neighbor rules for the current cell
        TileWFC currentTile = grid.Cells[cellPos.x, cellPos.y].Tile.GetComponent<TileWFC>();

        Vector2Int left = grid.GetLeftPosition(cellPos);
        Vector2Int right = grid.GetRightPosition(cellPos);
        Vector2Int up = grid.GetUpPosition(cellPos);
        Vector2Int down = grid.GetDownPosition(cellPos);

        if (left != cellPos) RemoveIncompatibleTiles(left, currentTile.LEFT);

        if (right != cellPos) RemoveIncompatibleTiles(right, currentTile.RIGHT);

        if (up != cellPos) RemoveIncompatibleTiles(up, currentTile.UP);

        if (down != cellPos) RemoveIncompatibleTiles(down, currentTile.DOWN);
    }

    // i.e Lowers etrophy
    private void RemoveIncompatibleTiles(Vector2Int pos, List<GameObject> validTiles)
    {
        List<GameObject> tilesToRemove = new List<GameObject>();

        // Collect tiles to remove
        foreach (GameObject tile in grid.Cells[pos.x, pos.y].Entropy)
        {
            if (!validTiles.Contains(tile))
            {
                tilesToRemove.Add(tile);
            }
        }

        //UnityEngine.Debug.Log("Removed: " + tilesToRemove.Count +" tiles");

        // Remove non-valid tiles
        foreach (GameObject tile in tilesToRemove)
        {
            grid.Cells[pos.x, pos.y].Entropy.Remove(tile);
        }
    }

    // Set valid exits of the generated map
    public void SetExits()
    {
        for (int x = 0; x < grid.Rows; x++)
        {
            for (int y = 0; y < grid.Columns; y++)
            {
                var cellPos = new Vector2Int(x, y);
                var tilewfc = grid.Cells[x, y].Tile.GetComponent<TileWFC>();

                tilewfc.Exit = false;

                // Check if any of the valid connections would lead to an exit
                if (tilewfc.availablePaths.Contains(TileWFC.Direction.Up) && cellPos.x == 0 ||
                    tilewfc.availablePaths.Contains(TileWFC.Direction.Down) && cellPos.x == grid.Rows - 1 ||
                    tilewfc.availablePaths.Contains(TileWFC.Direction.Left) && cellPos.y == 0 ||
                    tilewfc.availablePaths.Contains(TileWFC.Direction.Right) && cellPos.y == grid.Columns - 1)
                {
                    tilewfc.Exit = true;
                    //UnityEngine.Debug.LogError($"Exit at {cellPos}");
                }
            }
        }
    }


    public void SpawnGridCells(Grid grid, bool solution = false)
    {
        if (grid == null)
        {
            UnityEngine.Debug.LogError("Grid is not initialized.");
            return;
        }

        for (int x = 0; x < grid.Rows; x++)
        {
            for (int z = 0; z < grid.Columns; z++)
            {
                GridCell cell = grid.Cells[x, z];

                // Instantiate the selected tile
                GameObject spawnedTile = Instantiate(cell.Tile);
                BoxCollider collider = spawnedTile.GetComponent<BoxCollider>();

                if (collider != null)
                {
                    // Use the center of the BoxCollider for positioning
                    Vector3 position = new Vector3(x * collider.size.x + collider.size.x / 2, 0, z * collider.size.z + collider.size.z / 2);
                    spawnedTile.transform.position = position + (solution? new Vector3(0, 0, (grid.Columns + 1) * collider.size.z) : Vector3.zero);

                    var tilewfc = spawnedTile.GetComponent<TileWFC>();

                    if(grid.id == 0)
                    {
                        cell.x = z;
                        cell.y = x;
                        cell.isStreet = tilewfc.IsStreet;
                        cell.tileID = tilewfc.ID;

                        cell.directions = new List<TileWFC.Direction>(tilewfc.availablePaths);
                    }

                    grid.Cells[x, z].Tile = spawnedTile;
                }

                else
                {
                    UnityEngine.Debug.LogError($"Tile at cell ({x}, {z}) is missing its BoxCollider Component.");
                }
            }
        }
    }
}
