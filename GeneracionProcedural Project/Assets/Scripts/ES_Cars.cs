//using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using TMPro;

public class ES_Cars : MonoBehaviour
{
    public GameObject startGoalPrefab;
    public GameObject carPrefab;

    [Range(0.0f, 1.0f)]
    public float carSpawnRate = 0.2f;

    public int population;
    public int lambda;
    public int maxGenerations;

    WFC wfc;
    WFC.Grid solucion = null;

    [SerializeField] TextMeshProUGUI initFitnessTMP;
    [SerializeField] TextMeshProUGUI finalFitnessTMP;

    public void StartES(WFC wfc)
    {
        this.wfc = wfc;
        InstantiateCars();

        Resolve();
        Debug.Log($"Mapa\n{solucion}");
        SpawnSolution();
    }

    void InstantiateCars()
    {
        bool startSet = false;
        WFC.GridCell firstStreet = null;
        WFC.GridCell lastStreet = null;
        GameObject lastCar = null;

        int streetCount = 0;

        foreach (WFC.GridCell cell in wfc.grid.Cells)
        {
            TileWFC tile = cell.Tile.GetComponent<TileWFC>();

            if (cell.isStreet)
            {
                streetCount++;
                lastStreet = cell;

                if (!startSet)
                {
                    firstStreet = cell;
                    cell.isStart = true;
                    startSet = true;
                }
                else
                {
                    if(Random.Range(0.0f, 1.0f) < carSpawnRate)
                    {
                        cell.hasCar = true;
                        tile.hasCar = true;
                        lastCar = Instantiate(carPrefab, cell.Tile.transform);
                    }
                }
            }
        }

        Debug.LogWarning($"TotalStreets: {streetCount}");

        if (lastStreet is null || lastStreet.isStart)
        {
            Debug.LogError("No se pudo asignar un inicio y un final.");
            return;
        }

        lastStreet.isGoal = true;

        if(lastStreet.hasCar) 
        { 
            Destroy(lastCar);

            lastStreet.hasCar = false;
        }

        Instantiate(startGoalPrefab, firstStreet.Tile.transform);
        Instantiate(startGoalPrefab, lastStreet.Tile.transform);
    }

    void Resolve()
    {
        if (lambda > population - lambda) { Debug.LogError("SOL. TEMPORAL: Lambda debe ser menor a total - lambda\n"); }

        //Revisar fitness estado inicial
        float firstFitness = Fitness(wfc.grid);
        initFitnessTMP.text = $"Fitness inicial: {firstFitness}";

        if(firstFitness == 1.0f)
        {
            Debug.Log("Ya estaba resuelto");
            solucion = wfc.grid;
            finalFitnessTMP.text = $"Fitness final: {firstFitness}";
            return;
        }

        //PriorityQueue<(WFC.Grid, float), float> minQ = new PriorityQueue<(WFC.Grid, float), float>(); // Prioridades reales.
        //PriorityQueue<(WFC.Grid, float), float> maxQ = new PriorityQueue<(WFC.Grid, float), float>(); // Prioridades inversas. Para etapa de clonaci�n.

        //List<WFC.Grid> grids = new List<WFC.Grid>();
        List<WFC.Grid> min = new List<WFC.Grid>();
        List<WFC.Grid> max = new List<WFC.Grid>();
        //Queue<(WFC.Grid, float)> auxQ = new Queue<(WFC.Grid, float)>(); // Auxiliar. Guarda valores temporalmente.

        Debug.Log($"GENERACION 1");
        for(int i = 0; i < population; i++)
        {
            WFC.Grid child = new WFC.Grid(wfc.grid);

            //Desordenar
            child.MoveOnceRandomly();
            //Calcular fitness
            float fitness = Fitness(child);
            child.ESFitness = fitness;
            // Caso victoria
            if (fitness == 1.0f)
            {
                solucion = child;
                return;
            }

            AddAndSort(child, min);
            AddAndSort(child, max, false);
        }

        for(int i = 0; i < maxGenerations; i++)
        {
            Debug.Log($"GENERACION {i+2}");
            for (int j = 0; j < lambda; j++)
            {
                min.RemoveAt(0);
            }

            while(min.Count < population)
            {
                //Debug.LogWarning($"max count: {max.Count}");
                WFC.Grid candidate = new WFC.Grid(max[0]); max.RemoveAt(0);
                //Desorden
                candidate.MoveOnceRandomly();
                // Fitness
                float fitness = Fitness(candidate);
                candidate.ESFitness = fitness;
                // Caso victoria
                if (fitness == 1.0f)
                {
                    solucion = candidate;
                    finalFitnessTMP.text = $"Fitness final: {fitness}";
                    return;
                }
                AddAndSort(candidate, min);
            }

            max.Clear();

            foreach(WFC.Grid g in min)
            {
                AddAndSort(g, max, false);
            }
        }

        WFC.Grid mejor = max[0];

        // Entrega el mejor resultado si no hay solucion
        solucion = mejor;


        finalFitnessTMP.text = $"Fitness final: {Fitness(solucion)}";
    }

    
    float Fitness(WFC.Grid g)
    {
        float factorDistancia, factorDistanciaIF, factorDistanciaFI, factorDistanciaCercanos;
        const float pesoDistancia = 1.0f;

        factorDistanciaIF = CalcularFactorDistancia(g, out WFC.GridCell cercanoIF);
        factorDistanciaFI = CalcularFactorDistancia(g, out WFC.GridCell cercanoFI, true);
        factorDistanciaCercanos = CalcularFactorDistancia(g, cercanoIF, cercanoFI);

        //factorDistancia = Mathf.Max(CalcularFactorDistancia(g), CalcularFactorDistancia(g, true));
        factorDistancia = Mathf.Max(factorDistanciaIF, factorDistanciaFI, factorDistanciaCercanos);

        float fitness = factorDistancia * pesoDistancia;
        Debug.Log($"FITNESS GRID {g.id}: {fitness}");

        return fitness;
    }

    float CalcularFactorDistancia(WFC.Grid g, out WFC.GridCell nodoCercano, bool inverso = false)
    {
        //Debug.LogError("NO IMPLEMENTADO"); return 0;
        WFC.GridCell inicio = null, fin = null;



        foreach(WFC.GridCell cell in g.Cells)
        {
            //TileWFC tile = cell.Tile.GetComponent<TileWFC>();
            

            if (inverso)
            {
                if (cell.isStart) fin = cell;
                if (cell.isGoal) inicio = cell;
            }
            else
            {
                if (cell.isStart) inicio = cell;
                if (cell.isGoal) fin = cell;
            }
        }
        
        

        int distX = Mathf.Abs(inicio.x - fin.x);
        int distY = Mathf.Abs(inicio.y - fin.y);

        float maxDistancia = Mathf.Sqrt(distX * distX + distY * distY);
        float menorDistancia = maxDistancia;
        nodoCercano = inicio;

        Queue<WFC.GridCell> porRevisar = new Queue<WFC.GridCell>();

        porRevisar.Enqueue(inicio);
        inicio.explorado = true;

        while (porRevisar.Count != 0)
        {
            WFC.GridCell actual = porRevisar.Dequeue();
            foreach (WFC.GridCell vecino in Vecinos(actual, g))
            {
                // Condici�n de salida
                if (vecino == fin)
                {
                    FinalizarExploracion(g);
                    return 1.0f;
                }

                if (vecino.explorado || !vecino.isStreet || vecino.hasCar) continue;

                int difX = Mathf.Abs(vecino.x - fin.x);
                int difY = Mathf.Abs(vecino.y - fin.y);
                float dist = Mathf.Sqrt(difX * difX + difY * difY);
                if (dist < menorDistancia)
                {
                    menorDistancia = dist;
                    nodoCercano = vecino;
                }

                porRevisar.Enqueue(vecino);
                vecino.explorado = true;
            }
        }

        FinalizarExploracion(g);
        float valor = 1.0f - menorDistancia / maxDistancia;

        return valor;
        
    }

    float CalcularFactorDistancia(WFC.Grid g, WFC.GridCell cercanoIF, WFC.GridCell cercanoFI)
    {
        WFC.GridCell inicio = null, fin = null;

        foreach (WFC.GridCell cell in g.Cells)
        {

            if (cell.isStart) inicio = cell;
            if (cell.isGoal) fin = cell;
        }

        int maxDistX = Mathf.Abs(inicio.x - fin.x);
        int maxDistY = Mathf.Abs(inicio.y - fin.y);
        float maxDistancia = Mathf.Sqrt(maxDistX * maxDistX + maxDistY * maxDistY);

        int distX = Mathf.Abs(cercanoIF.x - cercanoFI.x);
        int distY = Mathf.Abs(cercanoIF.y - cercanoFI.y);
        float distancia = Mathf.Sqrt(distX * distX + distY * distY);

        float valor = 1.0f - distancia / maxDistancia;

        return valor;
    }

    List<WFC.GridCell> Vecinos(WFC.GridCell nodo, WFC.Grid g)
    {
        List<WFC.GridCell> vecinos = new List<WFC.GridCell>();
        /*
        if (nodo.x > 0 )
        {
            vecinos.Add(g.Cells[nodo.x - 1, nodo.y]);
        }
        if (nodo.x < g.Columns - 1) vecinos.Add(g.Cells[nodo.x + 1, nodo.y]);
        if (nodo.y > 0) vecinos.Add(g.Cells[nodo.x, nodo.y - 1]);
        if (nodo.y < g.Rows - 1) vecinos.Add(g.Cells[nodo.x, nodo.y + 1]);
        */
        if (nodo.x > 0 && nodo.directions.Contains(TileWFC.Direction.Left))
        {
            vecinos.Add(g.Cells[nodo.y, nodo.x - 1]);
        }
        if (nodo.x < g.Columns - 1 && nodo.directions.Contains(TileWFC.Direction.Right))
        {
            vecinos.Add(g.Cells[nodo.y, nodo.x + 1]);
        }
        if (nodo.y > 0 && nodo.directions.Contains(TileWFC.Direction.Up))
        {
            vecinos.Add(g.Cells[nodo.y - 1, nodo.x]);
        }
        if (nodo.y < g.Rows - 1 && nodo.directions.Contains(TileWFC.Direction.Down))
        {
            vecinos.Add(g.Cells[nodo.y + 1, nodo.x]);
        }

        return vecinos;
    }

    void FinalizarExploracion(WFC.Grid g)
    {
        for (int y = 0; y < g.Rows; y++)
        {
            for (int x = 0; x < g.Columns; x++)
            {
                g.Cells[y, x].explorado = false;
            }
        }
    }

    void SpawnSolution()
    {
        wfc.SpawnGridCells(solucion, true);
        foreach(WFC.GridCell cell in solucion.Cells)
        {
            if(cell.isStart || cell.isGoal)
            {
                Instantiate(startGoalPrefab, cell.Tile.transform);
            }
            else if(cell.hasCar)
            {
                Instantiate(carPrefab, cell.Tile.transform);
            }
        }
    }

    void AddAndSort(WFC.Grid newGrid, List<WFC.Grid> newList, bool minToMax = true)
    {
        newList.Add(newGrid);
        Sort(newList, minToMax);
    }

    void Sort(List<WFC.Grid> newList, bool minToMax = true)
    {
        int d = minToMax ? 1 : -1;
        newList.Sort(delegate (WFC.Grid g1, WFC.Grid g2)
        {
            return d * g1.CompareTo(g2);
        });
    }
}
